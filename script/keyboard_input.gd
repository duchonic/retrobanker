extends Node


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if Input.is_action_just_released("ui_right"):
		print("ui_right")
	if Input.is_action_just_released("ui_left"):
		print("ui_left")
	if Input.is_action_just_released("ui_up"):
		print("ui_up")
	if Input.is_action_just_released("ui_down"):
		print("ui_down")
	if Input.is_action_just_released("ui_select"):
		print("ui_select")
	if Input.is_action_just_released("ui_cancel"):
		print("ui_cancel")

func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.is_pressed():
			print("Key pressed: ", event.keycode)
		else:
			print("Key released: ", event)
