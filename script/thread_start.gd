extends Node

var thread_a: Thread
var thread_b: Thread
var thread_c: Thread
var thread_d: Thread

signal start_finished


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	thread_a = Thread.new()
	thread_b = Thread.new()
	thread_c = Thread.new()
	thread_d = Thread.new()
	thread_c.start(thread_start.bind("c", 0.001), Thread.PRIORITY_NORMAL)
	thread_d.start(thread_start.bind("d", 0.001), Thread.PRIORITY_LOW)
	thread_a.start(thread_start.bind("a", 0.001), Thread.PRIORITY_HIGH)
	thread_b.start(thread_start.bind("b", 0.001), Thread.PRIORITY_NORMAL)

func thread_start(name, delay):
	for time in range(0, 10):
		var current_time = Time.get_unix_time_from_system()
		while (current_time + delay) > Time.get_unix_time_from_system():
			#await get_tree().create_timer(0.001).timeout
			pass
		print("thread " , name, "  running...", time, " time:", current_time)
	print("exit thread ", name)
	start_finished.emit()
	
func _exit_tree() -> void:
	print("exit tree")
	thread_a.wait_to_finish()
	thread_b.wait_to_finish()
	thread_c.wait_to_finish()
	thread_d.wait_to_finish()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass
